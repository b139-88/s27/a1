const http = require('http');
const PORT = 4000;

let courses = [{
    courseName: "ReactJS",
    isActive: true
}, {
    courseName: "VueJS",
    isActive: true
}, {
    courseName: "Tailwind CSS",
    isActive: false
}];


http.createServer((req, res) => {
    if (req.url === '/' && req.method === 'GET') {
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        res.write("Welcome to Booking System");
        res.end();
    } else if  (req.url === '/profile' && req.method === 'GET') {
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        res.write("Welcome to your profile!");
        res.end();
    } else if  (req.url === '/courses' && req.method === 'GET') {
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        res.write("Here's our courses available.");
        res.end();
    } else if  (req.url === '/addcourse' && req.method === 'POST') {
        res.writeHead(200, { 'Content-Type': 'text/plain' });

        let reqBody = "";

        req.on("data", (data) => reqBody += data);

        req.on("end", () => {
            reqBody = JSON.parse(reqBody);
            courses.push(reqBody);
            res.write("Add course to our resources.");
            res.end();
            console.log(courses);
        });
    } else if  (req.url === '/updatecourse' && req.method === 'PUT') {
        res.writeHead(200, { 'Content-Type': 'text/plain' });

        let reqBody = "";

        req.on("data", (data) => reqBody += data);

        req.on("end", () => {
            reqBody = JSON.parse(reqBody);
            
            courses.find((course) => {
                if (course.courseName === reqBody.courseName) {
                    course.isActive = reqBody.isActive;
                }
            });

            res.write("Update a course to our resources.");
            res.end();
            console.log(courses);
        });
    } else if  (req.url === '/archivecourse' && req.method === 'DELETE') {
        res.writeHead(200, { 'Content-Type': 'text/plain' });

        let reqBody = "";

        req.on("data", (data) => reqBody += data);

        req.on("end", () => {
            reqBody = JSON.parse(reqBody);
            
            courses.find((course) => {
                if (course.courseName === reqBody.courseName) {
                    courses.pop(reqBody.courseName);
                }
            });

            res.write("Archive a course to our resources.");
            res.end();
            console.log(courses);
        });
    } else {
        res.writeHead(404, { 'Content-Type': 'text/plain' });
        res.write("Page not found.");
        res.end();
    }

}).listen(PORT, () => console.log(`Server listening on ${PORT}`));